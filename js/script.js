$(document).ready(function () {
 
    $('.menu__nav li').hover(
      function() {
        $('ul', this).slideDown(110);
      },
      function() {
        $('ul', this).slideUp(110);
      }
    );
   


    t = $("html");

    var c = $("#menu").mmenu({
        extensions: {
            all: ["theme-white", "fx-menu-slide", "pagedim-black"]
        },
        navbar: {
            title: "Меню"
        },
        pageScroll: {
            scroll: true,
            update: true,
            scrollOffset: 300
        }
    }).data("mmenu"),
        d = $("#hamburger").on("click", function (e) {
            e.preventDefault(), t.hasClass("mm-opened") ? c.close() : c.open()
        }).children(".hamburger");
    c.bind("close:finish", function () {
        setTimeout(function () {
            d.removeClass("is-active")
        }, 100)
    }), c.bind("open:finish", function () {
        setTimeout(function () {
            d.addClass("is-active")
        }, 100)
    })

  });

  